#!/bin/sh
echo "CREATE DATABASE IF NOT EXISTS db; USE db;" > /tmp/init.sql

/usr/bin/mysqld &
sleep 5
mysql < /tmp/init.sql

liquibase --changeLogFile=/tmp/changelog.xml migrate